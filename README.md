
# Nuclei segmentation with StarDist, a user-friendly deep-learning tool for the biologist

## Abstract 

Automated accurate segmentation of nuclei on bioimage is one if not the most often required task in image analysis project in core facilities and research laboratories yet it is still a complex and a time-consuming task with classical approaches. The advent of new deep learning-based tools that are easy to use by biologists without any Deep Learning (DL) expertise has been a game changer.   

We will use out-of-the-box StarDist as an example tool to guide beginners through their first use of their first DL cell nuclei segmentation using open source software Fiji and QuPath. The workshop will present 1 an overview of the limitations of classical segmentation approaches and the advantages of StarDist for this task, 2 demonstrate how to apply the DL tool to the segmentation tasks and 3 most importantly how to evaluate the results and identify current limits to the out-o-the-box DL approach.  

## Educational goal

Participants do not need pre-requisite to follow the workshop. The workshop is targeted to complete deep learning novices as well as to non-experts looking for guidance in the critical assessment of the results.  

We will use the open-source software Fiji and QuPath as they are both ubiquitously used in core facilities and research laboratories, and as such will maximise the skill transfer impact of this workshop enabling participants to **apply the newly learned skills to their own data back in their host institution**.  

The workshop has 3 clear educational goals: demonstrate when to use the DL tools, how to correctly use the DL tools, and how to **critically evaluate** the DL tools.  

In introduction participants will be reminded of the classical intensity based segmentation approach for cell nuclei segmentation by running provided Fiji/QuPath pipelines: image filtering/intensity based thresholding/binary operation/objects labelling. Several example bioimages will be provided to **demonstrate when this approach is successful and when it does fail**.   

After a quick overview of the StarDist Plugin installation in both Fiji & QuPath, participants will learn how to run StarDist on the fluorescent/brightfield microscopy demo images in both software. We will detail the image parameters (frame size, object size, image type, etc. ) required to run StarDist and how to use data pre-processing/formatting for proper use of StarDist.  

In the final part, the participants **will learn to critically assess the segmentation results** of both classical and DL approaches to enable them to make the right tool choice. For this we will use the recently developed Fiji Plugin MiC (unpublished, Cédric Messaoudi, Institut Curie). The participants should leave the workshop with a clear understanding of the principles of ground truth and of some of the quality control metrics (IoU, precision, accuracy).

# Workshop timeline

* Introduction to StarDist, 10 minutes
* StarDist plugin installation in Fiji, 5 minutes
* Nuclei segmentation with StarDist plugin in Fiji, 20 minutes
* StarDist extension installation in QuPath, 5 minutes 
* Nuclei segmentation with StarDist in QuPath, 20 minutes
* Comparison of segmentation images with quality controls tools (Fiji), 25 minutes.
* Final discussions, 10 minutes

# TP Stardist 

  To see TP Stardist in Mifobio clique [ici](https://forgemia.inra.fr/gt-maiia/mifobio_stardist/-/wikis/home)



